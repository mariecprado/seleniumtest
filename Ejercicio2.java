package functionalTests;

import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.Test;
import auxClasses.Browsers;
import auxClasses.ReadUrlFile;
import auxClasses.WindowHandler;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.WebElement;


import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;

//import org.openqa.selenium.support.ui.Select;
/*
 * Test if selenium has been set up properly.
 * @author Pablo Miranda
 *
 */
// Browsers is a class that initiates the WebDriver depending on the parameter passed by the build.xml file. 
// Otherwise it has a default value that can me modified at auxClasses/Browsers at the @Optional tag
public class Ejercicio2 extends Browsers{

	@Test
	public void f() {
		try {
			
			driver.get("http://compras135.ufm.edu/");
			
			ReadUrlFile.Wait(2000);
			driver.findElement(By.cssSelector("button")).click();
			WebElement email = driver.findElement(By.name("identifier"));
			Actions builder = new Actions(driver);
	        Actions seriesOfActions = builder.moveToElement(email).click().sendKeys(email, "xik");
	        seriesOfActions.perform();
	        driver.findElement(By.id("identifierNext")).click();
	        ReadUrlFile.Wait(3000);
	        WebElement passw = driver.findElement(By.cssSelector("input[class='whsOnd zHQkBf']"));
	        Actions builderp = new Actions(driver);
	        Actions seriesOfActionsp = builderp.moveToElement(passw).click().sendKeys(passw, "xik$2015");
	        seriesOfActionsp.perform();
	        driver.findElement(By.id("passwordNext")).click();
	        ReadUrlFile.Wait(5000);
	        driver.findElement(By.cssSelector("img[src='themes/ufm/images/logo-ufm.png']")).click();
	        driver.get("http://compras135.ufm.edu/index.php");
	        Actions actions = new Actions(driver); 
	        WebElement menuHoverLink = driver.findElement(By.cssSelector("td[menu='menu_2']"));
	        Actions seriesOfActionsmH = actions.moveToElement(menuHoverLink).click();
	        seriesOfActionsmH.perform();
	        actions.moveToElement(menuHoverLink).perform();
	        driver.findElement(By.linkText("Administración académica")).click();
	        ReadUrlFile.Wait(2000);
	        Actions actionsCA = new Actions(driver); 
	        WebElement menuHoverLinkCA = driver.findElement(By.id("tdItemMenu_4"));
	        Actions seriesOfActionsCA = actionsCA.moveToElement(menuHoverLinkCA).click();
	        seriesOfActionsCA.perform();
	        actionsCA.moveToElement(menuHoverLinkCA).perform();
	        driver.findElement(By.linkText("Administración de LOGOS")).click();
	        driver.findElement(By.id("buttClassListadoLogosNuevaSolicitud")).click();
	        WindowHandler.next(driver);
	        ReadUrlFile.waitForLoad(driver);
	        Actions builderNF = new Actions(driver);
	        WebElement nameNF= driver.findElement(By.id("txtNombre"));
	        Actions seriesOfActionsNF = builderNF.moveToElement(nameNF).click().sendKeys(nameNF, "Karl Heinz Chávez Asturias");
	        seriesOfActionsNF.perform();
	        Actions builderCN = new Actions(driver);
	        WebElement nameCN = driver.findElement(By.id("txtCarne"));
	        Actions seriesOfActionsCN = builderCN.moveToElement(nameCN).click().sendKeys(nameCN, "20100057");
	        seriesOfActionsCN.perform();
	        Select dropdownrol = new Select(driver.findElement(By.id("sltLogoOrigen")));
	        dropdownrol.selectByVisibleText("Catedrático");
	        
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='tdSltLogoActividad']/button"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='tdSltLogoActividad']/div[1]/ul/li[2]/label/span"))).click();
	        
	        Actions builderAct = new Actions(driver);
	        WebElement nameAct = driver.findElement(By.xpath("//*[@id='txtNombreActividad']"));
	        Actions seriesOfActionsAct = builderAct.moveToElement(nameAct).click().sendKeys(nameAct, "TestXik_Prado01");
	        seriesOfActionsAct.perform();
	        
	        Actions builderDesc = new Actions(driver);
	        WebElement nameDesc = driver.findElement(By.id("txtDescripcion"));
	        Actions seriesOfActionsDesc = builderDesc.moveToElement(nameDesc).click().sendKeys(nameDesc, "Nuevo Logo");
	        seriesOfActionsDesc.perform();
	        Actions builderCant= new Actions(driver);
	        WebElement nameCant = driver.findElement(By.name("txtLogoAcredita"));
	        Actions seriesOfActionsCant = builderCant.moveToElement(nameCant).click().sendKeys(nameCant, "5");
	        seriesOfActionsCant.perform();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='divEditSubCiclo']/button"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='divEditSubCiclo']/div/ul/li[3]/label/span"))).click();
	        Actions builderCupo= new Actions(driver);
	        WebElement nameCupo = driver.findElement(By.name("txtCupo_1_1"));
	        Actions seriesOfActionsCupo = builderCupo.moveToElement(nameCupo).click().sendKeys(nameCupo, "15");
	        seriesOfActionsCupo.perform();
	        Actions builderSecc= new Actions(driver);
	        WebElement nameSecc = driver.findElement(By.id("txtSeccion_1_1"));
	        Actions seriesOfActionsSecc = builderSecc.moveToElement(nameSecc).click().sendKeys(nameSecc, "A");
	        seriesOfActionsSecc.perform();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buttSendSolicitud']"))).click();
	        WebElement menuHoverLink2 = driver.findElement(By.cssSelector("td[menu='menu_2']"));
	        Actions actions2 = new Actions(driver); 
	        Actions seriesOfActionsmH2 = actions2.moveToElement(menuHoverLink2).click();
	        seriesOfActionsmH2.perform();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='tdItemMenu_4']"))).click();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='externo_4_16']/tbody/tr/td/a"))).click();
	        //wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='txtListadoLogo_Titulo']"))).click();
	        Actions builderTitulo= new Actions(driver);
	        WebElement nameTitulo = driver.findElement(By.xpath("//*[@id='txtListadoLogo_Titulo']"));
	        Actions seriesOfActionsTitulo = builderTitulo.moveToElement(nameTitulo).click().sendKeys(nameTitulo, "TestXik_Prado1");
	        seriesOfActionsTitulo.perform();
	        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[@id='buttClassListadoLogosBuscar']"))).click();
	        ReadUrlFile.Wait(5000);
		} catch (Exception e) {
			Assert.fail(e.getMessage());
		}
	}
}
